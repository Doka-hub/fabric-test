from celery import Celery

import os


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fabric_test.settings')


celery_app = Celery('fabric_test', result_expires=60)
celery_app.config_from_object('django.conf:settings')

celery_app.autodiscover_tasks()
