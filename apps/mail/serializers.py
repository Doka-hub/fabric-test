from rest_framework import serializers

from .models import Mail, Client, Message


class MailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mail
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class MessageStatisticSerializer(serializers.ModelSerializer):
    send_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = Message
        fields = ['send_count', 'status']
