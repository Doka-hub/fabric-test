import factory
from factory.django import DjangoModelFactory

from .models import Client


class ClientFactory(DjangoModelFactory):
    phone_number = factory.Faker('phone_number')
    mobile_operator_code = factory.Faker('country_calling_code')
    tag = factory.Faker('prefix')
    timezone = factory.Faker('timezone')

    class Meta:
        model = Client
