from django.db import transaction
from django.core.management.base import BaseCommand
from django.conf import settings

from django_celery_beat.models import CrontabSchedule, PeriodicTask

import zoneinfo

from apps.mail.models import Client
from apps.mail.factories import ClientFactory


class Command(BaseCommand):
    NUM_CLIENTS = 100

    @transaction.atomic
    def handle(self, *args, **kwargs):
        self.stdout.write("Deleting old data...")

        models = [Client]
        for m in models:
            m.objects.all().delete()

        self.stdout.write("Creating new data...")

        for _ in range(self.NUM_CLIENTS):
            ClientFactory()

        self.stdout.write("New data created...")

        schedule, _ = CrontabSchedule.objects.get_or_create(
            minute='0',
            hour='12',
            day_of_week='*',
            day_of_month='*',
            month_of_year='*',
            timezone=zoneinfo.ZoneInfo(settings.TIME_ZONE),
        )

        task, _ = PeriodicTask.objects.get_or_create(
            crontab=schedule,
            name='Send statistic',
            task='send_statistic',
        )
