from django.db import models
from django.db.models import Count
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from django.utils.timezone import datetime, timedelta


class Mail(models.Model):
    text = models.TextField(verbose_name='Текст')
    filter_client_attrs = models.CharField(
        max_length=255,
        verbose_name='Фильтр по свойствам клиентов',
    )
    start_date_mail = models.DateTimeField(
        verbose_name='Дата и время запуска рассылки',
    )
    end_date_mail = models.DateTimeField(
        verbose_name='Дата и время конца рассылки',
    )

    notify_to = models.EmailField(
        blank=True,
        null=True,
        verbose_name='Отправлять статистику на почту',
    )

    def __str__(self):
        return f'{self.id}'

    def get_message_list_by_status(self, fields: list[str] = None):
        if fields is None:
            fields = ['status']

        return self.messages.values(*fields).annotate(
            send_count=Count('status'),
        )


class Client(models.Model):
    phone_number = models.CharField(
        max_length=255,
        verbose_name='Номер телефона',
    )
    mobile_operator_code = models.CharField(
        max_length=10,
        verbose_name='Код мобильного оператора',
    )
    tag = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name='Тег',
    )
    timezone = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        verbose_name='Часовой пояс',
    )

    def __str__(self):
        return f'{self.id} - {self.phone_number}'


class Message(models.Model):
    STATUS_CHOICES = (
        ('send', 'send'),
        ('not_send', 'not_send'),
    )

    mail = models.ForeignKey(
        Mail,
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='Рассылка',
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='Клиенты',
    )
    date_mail = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name='Дата и время отправки',
    )
    status = models.CharField(
        max_length=255,
        choices=STATUS_CHOICES,
        default='not_send',
        verbose_name='Статус отправки',
    )

    def __str__(self):
        return f'{self.mail} | {self.client}'

    @property
    def prepared_data(self):
        return {
            'id': self.id,
            'phone': self.client.phone_number,
            'text': self.mail.text,
        }


@receiver(post_save, sender=Mail)
def send_mail(sender, created, instance: Mail, **kwargs):
    if created:
        from .tasks import send_mail as task_send_mail

        now = datetime.now(tz=instance.start_date_mail.tzinfo)

        if (
                now >= instance.start_date_mail
        ) and (
                now <= instance.end_date_mail
        ):
            task_send_mail.delay(instance.id)
        elif instance.start_date_mail > now:

            task_send_mail.apply_async(
                (instance.id,),
                eta=instance.start_date_mail,
            )
