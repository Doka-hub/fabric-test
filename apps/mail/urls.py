from django.urls import include, path

from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register(
    'mail',
    views.MailViewSet,
    basename='mail',
)
router.register(
    'client',
    views.ClientViewSet,
    basename='client',
)
router.register(
    'message',
    views.MessageViewSet,
    basename='message',
)

urlpatterns = [
    path('', include(router.urls)),
]
