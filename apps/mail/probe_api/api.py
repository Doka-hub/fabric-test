from django.conf import settings

from typing import Optional

import requests

import json


class ProbeAPI:
    BASE_URL = 'https://probe.fbrq.cloud/v1/'

    def __init__(
            self,
            token: str = settings.PROBE_TOKEN,
            base_url: Optional[str] = None,
    ):
        self.token = token

        if base_url:
            self.BASE_URL = base_url

    def _get_headers(self, is_auth: bool = True, **kwargs):
        headers = {
            'Content-Type': 'application/json',
            **kwargs,
        }
        if is_auth:
            headers['Authorization'] = f'Bearer {self.token}'

        return headers

    def _make_url(self, endpoint: str) -> str:
        return self.BASE_URL + endpoint

    def send(self, msg_id: int, data: dict) -> requests.Response:
        url = self._make_url(f'send/{msg_id}')
        headers = self._get_headers()

        response = requests.post(
            url,
            json.dumps(data),
            headers=headers,
        )
        return response
