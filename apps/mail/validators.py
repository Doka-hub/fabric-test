from django.core.exceptions import ValidationError
from django.utils import timezone


def date_mail_validator(value):
    if timezone.now() > value.astimezone(timezone.utc):
        raise ValidationError(
            'Дата отправки должна быть в будущем'
        )
