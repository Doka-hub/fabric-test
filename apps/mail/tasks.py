from django.db.models.query import Q
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.utils import timezone

from fabric_test.celery import celery_app

from .models import Mail, Client, Message
from .probe_api import ProbeAPI


@celery_app.task(name='send_mail')
def send_mail(mail_id):
    print('Рассылка запущена')
    mail = Mail.objects.get(id=mail_id)

    client_list = Client.objects.filter(
        Q(tag=mail.filter_client_attrs) |
        Q(mobile_operator_code=mail.filter_client_attrs),
    )

    messages = []
    for client in client_list:
        now = timezone.now()
        if not mail.end_date_mail < now:
            message, message_created = Message.objects.get_or_create(
                mail=mail,
                client=client,
            )

            if message.status == 'not_send':
                probe_api = ProbeAPI()
                response = probe_api.send(message.id, message.prepared_data)

                if response.status_code == 200:
                    message.date_mail = now
                    message.status = 'send'
                    messages.append(message)

    Message.objects.bulk_update(messages, ['date_mail', 'status'])


@celery_app.task(name='send_statistic')
def send_statistic():
    mail_list = Mail.objects.all()

    for mail in mail_list:
        message_list = mail.get_message_list_by_status()
        text = ''
        for message in message_list:
            text += f'ID рассылки: {mail.id}\n'
            text += f'Статус: {message["status"]}\n'
            text += f'Количество: {message["send_count"]}\n\n'
        msg = EmailMultiAlternatives(
            subject="statistic",
            from_email=settings.DEFAULT_FROM_EMAIL,
            body=text,
            to=[mail.notify_to]
        )
        msg.send()

