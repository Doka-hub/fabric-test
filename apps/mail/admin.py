from django.contrib import admin
from django.db.models import Count, Sum

from .models import Mail, Client, Message


class MessageInline(admin.TabularInline):
    model = Message

    readonly_fields = ['mail', 'client', 'date_mail', 'status']
    extra = 0


@admin.register(Mail)
class MailAdmin(admin.ModelAdmin):
    list_display = ['id', 'filter_client_attrs', 'start_date_mail',
                    'end_date_mail']

    inlines = [MessageInline]


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['id', 'mobile_operator_code', 'phone_number', 'tag',
                    'timezone']


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ['id', 'mail', 'client', 'date_mail', 'status']

    list_filter = [
        'mail__id',
    ]
