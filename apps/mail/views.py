from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Mail, Client, Message
from .serializers import (
    MailSerializer,
    ClientSerializer,
    MessageSerializer,
    MessageStatisticSerializer,
)


class MailViewSet(viewsets.ModelViewSet):
    queryset = Mail.objects.all()
    serializer_class = MailSerializer

    @action(detail=True, methods=['get'])
    def messages(self, request, *args, **kwargs):
        instance: Mail = self.get_object()
        message_list = instance.messages.all()
        message_serializer = MessageSerializer(message_list, many=True)
        return Response(message_serializer.data, status.HTTP_200_OK)

    @action(detail=True, methods=['get'], description='Статистика по рассылке')
    def statistic(self, request, *args, **kwargs):
        instance: Mail = self.get_object()
        message_list = instance.get_message_list_by_status()
        message_serializer = MessageStatisticSerializer(
            message_list,
            many=True,
        )
        return Response(message_serializer.data, status.HTTP_200_OK)


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    http_method_names = ['get']
