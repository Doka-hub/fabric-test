# Fabric Test Mailing

---

### Доп. задания

- 3 пункт. Проект запускается с помощью docker-compose
- 5 пункт. Добавлен swagger по адресу /docs/
- 6 пункт. Администраторский Web UI уже реализован в django
- 8 пункт. Каждое n-ое время запускается celery task, отправляющий статистику на почту, указанную при создании рассылки 
- 9 пункт. Задачи выполняются в фоне, не оказывая влияния на работу сервиса
---

### Quickstart
1. Скопировать .envs  
  Заполнить данные от почтового ящика  
  Указать TIMEZONE
```
cp .env.local.dist .env.local
cp .env.local.db.dist .env.local.db

EMAIL_HOST_USER=email@gmail.com
EMAIL_HOST_PASSWORD=password
TIMEZONE=timezone
``` 
2. Запустить
```
docker-compose -f docker-compose.local.yml up -d --build
```
3. Создать тестовые данные, администратора
```
docker exec -ti fabric_test_web python manage.py setup_test_data
docker exec -ti fabric_test_web python manage.py createsuperuser
```
4. Перейти на http://0.0.0.0:8002